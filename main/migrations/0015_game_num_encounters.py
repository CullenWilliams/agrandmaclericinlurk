# Generated by Django 2.1.2 on 2018-11-02 19:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0014_auto_20181031_1703'),
    ]

    operations = [
        migrations.AddField(
            model_name='game',
            name='num_encounters',
            field=models.IntegerField(default=5),
        ),
    ]
