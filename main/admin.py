from django.contrib import admin
from main.models import Game, UserProfile, Character

# Register your models here.
admin.site.register(Game)
admin.site.register(UserProfile)
admin.site.register(Character)