import datetime
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
# this is where our databases are setup (I think)
# helpfull list of Fields https://docs.djangoproject.com/en/2.1/ref/models/fields/#django.db.models.CharField


class UserProfile(models.Model):
    objects = models.Manager()

    user = models.OneToOneField(User, on_delete=models.CASCADE)

    v_name = models.CharField(max_length=32)
    bio = models.CharField(max_length=512, default="A new adventurer ")
    join_date = models.DateTimeField("date joined", default=timezone.now)
    email = models.EmailField(max_length=254)
    picture = models.ImageField(
        upload_to=None, height_field=None, width_field=None, max_length=100)


    def get_bio(self):
        return self.bio

    def __str__(self):
        return self.v_name

    def update_profile(self, up):
      self.v_name = up.v_name
      self.bio = up.bio
      self.save()



class Game(models.Model):
    objects = models.Manager()

    game_master = models.ForeignKey(
        UserProfile, related_name="game_master", null=True, on_delete=models.CASCADE)

    created_at = models.DateTimeField("date created", default=timezone.now)
    name = models.CharField(max_length=64, default="A super cool adventure")
    desc = models.CharField(max_length=1024, default="")
    ruleset = models.CharField(max_length=64, default="")
    ESRB = models.CharField(max_length=16, default="Exactly 3 years old")
    location = models.CharField(max_length=256, default="SPACE")
    max_players = models.IntegerField(default=4)
    int_current_players = models.IntegerField(default=0)
    num_encounters = models.IntegerField(default=5)
    # who want's pictures for the games too?

    players = models.ManyToManyField(UserProfile, related_name="in_games")
    pending_players = models.ManyToManyField(
        UserProfile, related_name="pending_players")

    def get_desc(self):
        return self.desc

    def __str__(self):
        return self.name
    
    def update_game(self, game):
        self.name = game.name
        self.desc = game.desc
        self.ruleset = game.ruleset
        self.ESRB = game.ESRB
        self.location = game.location
        self.max_players = game.max_players
        self.num_encounters = game.num_encounters
        self.save()

    def is_equal(self, game):
        if not(self.name == game.name):
            return False
        if not(self.desc == game.desc):
            return False
        if not(self.ruleset == game.ruleset):
            return False
        if not(self.ESRB == game.ESRB):
            return False
        if not(self.location == game.location):
            return False
        if not(self.max_players == game.max_players):
            return False
        if not(self.num_encounters == game.num_encounters):
            return False

class Character(models.Model):
    objects = models.Manager()

    owner = models.ForeignKey(UserProfile, related_name="user_character_set",null=True, on_delete=models.CASCADE)
    associated_game = models.ForeignKey(Game,related_name="game_character_set",on_delete=models.CASCADE)
    name = models.CharField(max_length= 64)
    bio = models.CharField(max_length= 512)
    # when we get to uploading pdfs a fileField may be usefull
    characterSheet = models.ImageField(
        upload_to=None, height_field=None, width_field=None, max_length=100)
    hasButt = models.BooleanField(default=True)

    def update_character(self, character):
        self.name = character.name
        self.bio = character.bio
        self.save()

    def set_butt(self, bool):
      self.hasButt=bool