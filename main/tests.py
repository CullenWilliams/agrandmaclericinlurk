from django.test import TestCase
from django.test import Client
from .models import UserProfile, Game,Character
from django.contrib.auth.models import User
from django.contrib.auth import logout

###################################### Model Tests ################################

class UserProfileTestCase(TestCase):
  def setUp(self):
    teddy=User.objects.create_user("Teddy")
    billy=User.objects.create_user("Billly")    
    UserProfile.objects.create(user = teddy,v_name="Bill", bio="Cool dude")
    UserProfile.objects.create(user = billy, v_name="Ted", bio="Whoa dude")

  def test_has_bio(self):
      """properly targets and checks bio"""
      bill = UserProfile.objects.get(v_name="Bill")
      ted = UserProfile.objects.get(v_name="Ted")
      self.assertEqual(bill.get_bio(), 'Cool dude')
      self.assertEqual(ted.get_bio(), 'Whoa dude')

  #tests update profile function used in the update game form
  def test_update_Profile(self):
    bill = UserProfile.objects.get(v_name="Bill")
    ted = UserProfile.objects.get(v_name="Ted")
    bill.update_profile(ted)
    self.assertEqual(ted.v_name, bill.v_name)
    self.assertEqual(ted.bio, bill.bio)


class GameTestCase(TestCase):
  def setUp(self):
    billy=User.objects.create_user("Billly")
    bill=UserProfile.objects.create(user = billy,v_name="Bill", bio="Cool dude")
    Game.objects.create(game_master=bill,name="Laird of The Rangs", desc="carry on my wayward son", ruleset="Flying", ESRB="Baby", location="Mordar", max_players=3, int_current_players=1,num_encounters=56)
    Game.objects.create(game_master=bill, name="How do GM?", desc="In a hole in the ground, there lived a Hobbit", ruleset="KOTOR", ESRB="G", location="bahamas", max_players=132, int_current_players=3,num_encounters=8)
  
  def test_set_correct(self):
      """properly targets and checks fields"""
      Laird = Game.objects.get(name="Laird of The Rangs")
      self.assertEqual(Laird.get_desc(), "carry on my wayward son")
      self.assertEqual(Laird.ruleset, "Flying")
      self.assertEqual(Laird.ESRB, "Baby")
      self.assertEqual(Laird.location, "Mordar")
      self.assertEqual(Laird.int_current_players,1)
      self.assertEqual(Laird.max_players, 3)
      self.assertEqual(Laird.num_encounters,56)
      self.assertEqual(Laird.game_master.v_name,"Bill")


  #tests update game function used in the update game form
  def test_update_game(self):
    Laird = Game.objects.get(name="Laird of The Rangs")
    Morrison = Game.objects.get(name="How do GM?")
    Laird.update_game(Morrison)
    self.assertEqual(Morrison.get_desc(), Laird.get_desc())
    self.assertEqual(Morrison.ruleset, Laird.ruleset)
    self.assertEqual(Morrison.ESRB, Laird.ESRB)
    self.assertEqual(Morrison.location, Laird.location)
    self.assertEqual(Morrison.name,Laird.name)
    self.assertEqual(Morrison.max_players, Laird.max_players)
    self.assertEqual(Morrison.num_encounters,Laird.num_encounters)
    

class CharacterTestCase(TestCase):
  def setUp(self):
    billy=User.objects.create_user("Billly")
    bill=UserProfile.objects.create(user = billy,v_name="Bill", bio="Cool dude")
    laird=Game.objects.create(game_master=bill,name="Laird of The Rangs", desc="carry on my wayward son", ruleset="Flying", ESRB="Baby", location="Mordar", max_players=1, int_current_players=3,num_encounters=56)
    Character.objects.create(owner=bill, associated_game=laird, name="Freddie Mercury", bio="Badass")
    Character.objects.create(owner=bill, associated_game=laird, name="Harriet Tubman", bio="Badass")

  def test_has_bio(self):
      """properly targets and checks"""
      bill = Character.objects.get(name="Freddie Mercury")
      ted = Character.objects.get(name="Harriet Tubman")
      self.assertEqual(bill.bio, 'Badass')
      self.assertEqual(ted.bio, 'Badass')

  def test_update_character(self):
    freddie = Character.objects.get(name="Freddie Mercury")
    hattie = Character.objects.get(name="Harriet Tubman")
    freddie.update_character(hattie)
    self.assertEqual(freddie.name, hattie.name)
    self.assertEqual(freddie.bio, hattie.bio)



############################### Route Tests #################################



class LoginTestCases(TestCase):
  def setUp(self):
    billy=User.objects.create_user("Billy")
    billy.set_password("NowNowBillyNotReally")
    billy.save()

  #assert the path of an incorrect log in works as expected
  def test_incorrect_login_path(self):
    client = Client()
    response = client.post('/accounts/login/',\
     {'username': 'Billy', 'password': 'Therealbilly'}, follow=True)
    self.assertEqual(response.status_code, 200)
    self.assertEqual(response.redirect_chain,[]) # leave on login if failed

  #asser the path of a correct log in works as expected
  def test_correct_login_path(self):
    client = Client()
    response = client.post('/accounts/login/',\
     {'username': 'Billy', 'password': 'NowNowBillyNotReally'}, follow=True)
    self.assertEqual(response.status_code, 200) 
    self.assertEqual(response.redirect_chain,
                    [('/main', 302),   #send to main 
                     ('/main/', 301)]) #301 means permanently moved

  #assert incorrect credentials do not give access
  def test_incorrect_credentials_do_not_login(self):
    client = Client()
    logged_in = client.login(username='Billy', password='Therealbilly')
    self.assertFalse(logged_in)

  #assert correct credentials give access
  def test_correct_credentials_login(self):
      client = Client()
      logged_in = client.login(username='Billy', password='NowNowBillyNotReally')
      self.assertTrue(logged_in)  

class CreateGameTestCases(TestCase):
  def setUp(self):
    billy=User.objects.create_user("Billy")
    billy.set_password("NowNowBillyNotReally")
    billy.save()
    UserProfile.objects.create(user=billy,v_name="Billie", bio="The best person")

  #assert the path of an incorrect log in works as expected
  def test_create_game(self):
    client = Client()
    bill = UserProfile.objects.get(v_name="Billie")
    client.login(username='Billy', password="NowNowBillyNotReally")    
    response = client.post("/main/create/",\
                           {'game_master':bill,
                            'name':"Path of Doom",
                            'desc':"carry on my wayward sun",
                            'ruleset':"Dying",
                            'ESRB':"Baby",
                            'location':"Mordar",
                            'max_players':1,
                            'int_current_players':3,
                            'num_encounters':56}, follow=True)
    #check for success                            
    self.assertEqual(response.status_code, 200)
    #check for appropriate redirect
    self.assertEqual(response.redirect_chain, [('/main', 302), ('/main/', 301)])
    #make sure game exists in model after request is complete  
    self.assertIsNotNone(Game.objects.get(name="Path of Doom"))

class DeleteCharacterTestCases(TestCase):
  def setUp(self):
    t=User.objects.create_user("Terrence")
    terrence=UserProfile.objects.create(user = t,v_name="TerryT", bio="Cool dude")
    the_dwarves=Game.objects.create(game_master=terrence,
                                    name="The Dwarves",
                                    desc="Some book on garrick's shelf",
                                    ruleset="Flying",
                                    ESRB="Baby",
                                    location="shelf",
                                    max_players=1,
                                    int_current_players=3,
                                    num_encounters=5)
    Character.objects.create(owner=terrence, associated_game=the_dwarves, name="Bob Eart", bio="AWOL soldier hunting that which killed his squad")
    
  #find and delete a character, then check to see if delete cascaded correctly
  def testDeleteCharacter(self):
    bob = Character.objects.get(name="Bob Eart")
    bobid=str(bob.id)
    client=Client()      
    response = client.post("/main/delete_character/"+bobid+"/",{},follow=True)
    self.assertEqual(response.status_code, 200)
    self.assertEqual(response.redirect_chain, [('/main/profile/', 302)])
    self.assertEqual(list(Character.objects.filter(name="Bob Eart")),[])


