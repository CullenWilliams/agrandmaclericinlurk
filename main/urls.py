from django.urls import path
from django.conf.urls import url
from . import views
#add the view to urlpatterns
app_name= "main"
urlpatterns = [
  path("",views.listing, name = "game"),
  path("register/", views.register,name="register"),
  path("create/", views.CreateGame.as_view(),name="create"),
  path("profile/", views.Profile.as_view(),name="profile"),
  path("game_overview/<int:pk>/",views.GameOverview.as_view(),name="game_overview"),
  path("join_request/<int:pk>/", views.JoinRequest.as_view(),name="join_request"),
  path("pending_chars/<int:pk>/", views.Pending.as_view() ,name="pending_characters"),
  path("remove_player/<int:pk>/", views.Remove.as_view() ,name="remove_player"),
  path("edit_game/<int:pk>/", views.EditGame.as_view(), name="edit_game"),
  path("create_character/",views.CreateCharacter.as_view(),name="create_character"),
  path("profile/edit/<int:pk>/",views.EditProfile.as_view(),name="edit"),
  path("delete_game/<int:pk>/", views.DeleteGame.as_view() ,name="delete_game"),
  path("delete_player/<int:pk>/", views.DeletePlayer.as_view() ,name="delete_player"),
  path("delete_character/<int:pk>/",views.DeleteCharacter.as_view(),name="delete_character"),
]
