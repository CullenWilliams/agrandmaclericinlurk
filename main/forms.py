from django import forms
from main.models import UserProfile, Game
from django.contrib.auth.models import User

#django authentication for username and password
class CreateUserForm(forms.ModelForm):
  #this formats the password to do the auto hide thing
  password = forms.CharField(widget=forms.PasswordInput())

  #the class meta provides the minimum for the form 
  class Meta:
    model = User
    #will add email in iteration 2 if we decide to have email enabled
    fields = ("username","password")

#form fields specific to out project
class CreateUserProfileForm(forms.ModelForm):
  class Meta:
    model = UserProfile
    fields = ("v_name",)
