from django import template

register = template.Library()

#this tag's functions are described in views.py under the class pending
@register.inclusion_tag("tags/pending_chars.html",takes_context=True)
def pending_chars(context):
  #we can figure out how to get that stuff over here later but it is problem because forms
  return context


@register.inclusion_tag("tags/remove_player.html",takes_context=True)
def remove_player(context):
  return context
  
@register.inclusion_tag("tags/join_game.html",takes_context=True)
def join_game(context):
  return context

@register.inclusion_tag("tags/create_character.html",takes_context=True)
def create_character(context):
  return context

@register.inclusion_tag("tags/delete_game.html",takes_context=True)
def delete_game(context):
  return context

@register.inclusion_tag("tags/delete_player.html",takes_context=True)
def delete_player(context):
  return context

@register.inclusion_tag("tags/delete_character.html",takes_context=True)
def delete_character(context):
  return context