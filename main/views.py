from django.core.paginator import Paginator
from django.shortcuts import render, render_to_response, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.views import generic, View
from django.urls import reverse
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import TemplateView
from django.contrib.auth import login, logout
from django.template import RequestContext
from django.utils import timezone
from django import template

from .models import UserProfile, Game, Character
from main.forms import CreateUserForm, CreateUserProfileForm

#----------------
# Main pages
#----------------

def listing(request):
  obj_list = Game.objects.order_by('-created_at')
  paginator = Paginator(obj_list,5)

  page = request.GET.get('page')
  games=paginator.get_page(page)
  return render(request,"main/index.html",{"games":games})

  
class Login(CreateView):
  template_name = "accounts/login.html"
  CreateView.model = UserProfile
  fields = '__all__'
  success_url="/main"


class Profile(TemplateView):
  template_name = "main/profile.html"


class GameOverview(generic.DetailView):
  model = Game
  context_object_name="game"
  template_name="main/game_overview.html"


#----------------
# Create views
#----------------
class CreateGame(CreateView):
  template_name = "main/create_game.html"
  CreateView.model = Game
  fields="name","desc","max_players","ruleset","ESRB","location","num_encounters"
  success_url="/main"
  def form_valid(self, form):
    game=form.save(commit=False)
    game.game_master = self.request.user.userprofile
    self.object=game
    self.object.save()
    return HttpResponseRedirect(self.get_success_url())

def register(request):
  context = RequestContext(request)
  registered = False
  if request.method=="POST":
    user_form = CreateUserForm(data=request.POST)
    profile_form = CreateUserProfileForm(data=request.POST)

    if user_form.is_valid() and profile_form.is_valid():
      user=user_form.save()

      #the set_password function hashes our password
      user.set_password(user.password)
      user.save()

      profile = profile_form.save(commit=False)
      profile.user = user
      profile.save()

      registered = True
      return HttpResponseRedirect("/accounts/login/",context)
  else:
    user_form = CreateUserForm()
    profile_form = CreateUserProfileForm()
    
  return render_to_response("main/register.html", {"user_form":user_form, "profile_form":profile_form,"registered":registered}, context)


#----------------
# Edit views
#----------------
class EditGame(UpdateView):
  template_name = "main/edit_game.html"
  model = Game
  fields="name","desc","max_players","ruleset","ESRB","location","num_encounters"

  def form_valid(self, form):
    if self.request.user.userprofile==self.object.game_master:
      game=form.save(commit=False)
      self.object.update_game(game)
      return HttpResponseRedirect("/main/game_overview/" + str(self.object.id))

class EditProfile(UpdateView):
  template_name = "main/edit_profile.html"
  model = UserProfile
  fields="v_name","bio",

  def form_valid(self,form):
    if self.request.user.userprofile==self.object:
      up=form.save(commit=False)
      self.object.update_profile(up)
      return HttpResponseRedirect("/main/profile/")



#class EditCharacter(UpdateView):
    

#----------------
# Tags
#----------------
class RequestView(generic.DetailView):
  template_name = "main/request.html"
  model = Game
  context_object_name = "game"
     
class Pending(UpdateView):
  template_name = "tags/pending_chars.html" # these are needed for form processing
  fields=["players",]
  model = Game
  def form_valid(self,form):
    state=self.request.POST.__getitem__("submit")
    if state=="Create":
      for p in self.request.POST.getlist("players"):
        u=UserProfile.objects.get(id=p)
        self.object.players.add(u)
        self.object.pending_players.remove(u)
        self.object.int_current_players+=1
        self.object.save()
    if state =="Destroy":
      for p in self.request.POST.getlist("players"):
        self.object.pending_players.remove(UserProfile.objects.get(id=p))
        self.object.save()
    return HttpResponseRedirect("/main/game_overview/" + str(self.object.id)) #this is how to redirect to same page

#handles removing user from game, inserted into game_overview
class Remove(UpdateView):
  template_name = "tags/remove_player.html" # these are needed for form processing
  fields=["players",]
  model = Game
  def form_valid(self,form):
    if (self.request.user.userprofile == self.object.game_master):
      for p in self.request.POST.getlist("players"):
        u=UserProfile.objects.get(id=p) # finds the user with the pending id
        self.object.players.remove(u) #removes that user from the game's players
        self.object.int_current_players-=1
        self.object.save()
      return HttpResponseRedirect("/main/game_overview/" + str(self.object.id)) #this is how to redirect to same page
    else:
      response = render_to_response("/error_codes/403.html")
      response.status_code = 403
      return response

class DeleteGame(DeleteView):
  template_name="tags/delete_game.html"
  fields=[]
  model = Game
  success_url="/main/"
  def form_valid(self,form):
    if (self.request.user.userprofile == self.object.game_master):
      self.object.delete()
      return HttpResponseRedirect(self.get_success_url())

class DeletePlayer(UpdateView):
  template_name="tags/delete_game.html"
  fields=[]
  model=UserProfile
  success_url="/main/"
  def form_valid(self,form):
      u = UserProfile.objects.get(id=self.request.user.id).user
      u.is_active=False
      u.delete()
      return HttpResponseRedirect(self.get_success_url())

class DeleteCharacter(DeleteView):
  template_name="tags/delete_character.html"
  fields=[]
  model = Character
  success_url="/main/profile/"
  def form_valid(self,form):
    if (self.request.user.userprofile == self.object.owner):
      self.object.delete()
      return HttpResponseRedirect(self.get_success_url())


class JoinRequest(UpdateView):
  template_name = "tags/request.html"
  model = Game
  fields=[]
  success_url="/main"
  def form_valid(self,form):

    game=self.object
    up=self.request.user.userprofile
    if up not in game.players.all():#guard
      game.pending_players.add(up)
      game.save()
    return HttpResponseRedirect(self.get_success_url())


class CreateCharacter(CreateView):
  template_name = "tags/create_character.html"
  fields=["name","bio","associated_game"]
  model = Character
  def form_valid(self,form):
    print("\n\nVALID\n\n")
    character=form.save(commit=False)
    character.owner = self.request.user.userprofile
    self.object=character
    self.object.save()
    return HttpResponseRedirect("/main/profile")
  def form_invalid(self,form):
    print("\n\nINVALID\n\n")
    for p in form:
      print(str(p))
      print("\n")
    return HttpResponseRedirect("/main/profile")

