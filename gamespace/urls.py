from django.urls import include, path
from django.contrib import admin


urlpatterns = [
  path("main/", include("main.urls")),
  path('accounts/', include('django.contrib.auth.urls')),
  path('admin/', admin.site.urls),
]

handler404 = 'main.views.handler404'
handler403 = 'main.views.handler403'